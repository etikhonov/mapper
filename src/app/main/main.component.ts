import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MapService } from '../services/map.service'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild('fileName') fileName: ElementRef;  
  @ViewChild('map') mapDiv: ElementRef;

  isClasteredFlag: boolean = true;

  constructor(private _mapService: MapService) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    this._mapService.initMap();
  }

  clasteredFlagChanged(event){
     this.isClasteredFlag = event.target.checked;
  }

  openTab(event, tabName) {
    let tabContents, tablinks;
    tabContents = document.getElementsByClassName("tabContent");
    for (let i = 0; i < tabContents.length; i++) {
      tabContents[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (let i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-border-amber", "");
    }
    document.getElementById(tabName).style.display = "block";
    event.currentTarget.firstElementChild.className += " w3-border-amber"; 
    this._mapService.resizeMap();
  }

  onFileChange(event){
    let file : File;
    let fileData;
    let srv =  this._mapService;
    if(event.target.files && event.target.files.length > 0) {
      file = event.target.files[0];
      this.fileName.nativeElement.value = file.name;
      let fr = new FileReader();
      fr.onloadend = ((ev) => {this.onFileParsed(fr);});
      fr.readAsText(file);
    } else {
      this.fileName.nativeElement.value = "";
    }
  }

  onFileParsed(event) {
    this._mapService.addToMap(event.result, this.isClasteredFlag);
  }

}
