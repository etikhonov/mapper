import { Injectable } from '@angular/core';

let L = require('leaflet');
var csv2geojson = require('csv2geojson');
require('leaflet.markercluster');

let map: any;

const defaultCoords: number[] = [55.7505, 37.6147]
const defaultZoom: number = 5;
//OpenStreetMap.Mapnik 
const urlTemplate: string = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
const attribution: string = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
const maxZoom: number = 19;

@Injectable()
export class MapService {

  constructor() { }

  initMap(){
    if(map == null) {
      map = L.map('map').setView(defaultCoords, defaultZoom);
      map.maxZoom = 100;
      L.tileLayer(urlTemplate, {
        attribution: attribution,
        maxZoom: maxZoom,
        minZoom: 5
      }).addTo(map);
    }
  }

  resizeMap(){map.invalidateSize();}

  addToMap(fileData, isClustered:boolean = true){
    csv2geojson.csv2geojson(fileData, { delimiter: ";"}, (err, data) => {
      if(isClustered) {
        this.addClusteredMarkers(err, data);
      } else {
        this.addMarkers(err, data);
      }
    });
  }

  addClusteredMarkers(err, data){
    if(err != null) { 
      console.log(err);
    } else {
      let features: any[] = data.features;
      let colors: any[] = [];
       
      for (var i=0; i < features.length; i++){
        let color = features[i].properties.color;
          if(colors.indexOf(color)== -1){
            colors.push(color);
          }
      } 
      for(var i=0; i < colors.length; i++)
      {
       let featureByColor: any[] = [];
        for (var j=0; j < features.length; j++)
        {
          if (colors[i] == features[j].properties.color)
          {
            featureByColor.push(features[j]);
          }
        }
        this.addClaster(featureByColor);
      }  
    }
  }

  private addMarkers(err, data) { 
    if(err == null)
    {
      L.geoJSON([data], {
        style: (feature) => {return feature.properties && feature.properties.style;},
        onEachFeature: (feature, layer) => this.onEachFeature(feature, layer),
        pointToLayer: (feature, latlng) => this.drawCircleMarker(feature, latlng)
      }).addTo(map);
      map.setView([data.features[0].geometry.coordinates[1], data.features[0].geometry.coordinates[0]], 5);
    }
    else {
      console.log(err);      
    }
  }

  private onEachFeature(feature, layer) {
    let popupContent = "";
    if (feature.properties && feature.properties.popupContent) {
		  popupContent += feature.properties.popupContent;
		}
    layer.bindPopup(popupContent);
  }

  private drawCircleMarker(feature, latlng) {
    if (feature.properties.color == "RLP") {
      return L.circle(latlng, {
              radius: 2000,
              color: '#990066',
              fillColor: '#DB709377',
              fillOpacity: 0.8,
      });
    }
    else {
      return L.circleMarker(latlng, {
              radius: 6,
              fillColor: feature.properties.color,
              color: "#000",
              weight: 1,
              opacity: 1,
              fillOpacity: 0.8
      });
    }
  }

  private createClasterIcon(cluster, features)
  { 
    var childCount = cluster.getChildCount();
		var c = ' marker-cluster-small';
    var childMarkers = cluster.getAllChildMarkers(),
			points = [],
			p, i;

		for (i = childMarkers.length - 1; i >= 0; i--) {
			p = childMarkers[i].getLatLng();
			points.push(p);
		}
    if (map.getZoom() == defaultZoom)
    {
       var layer = new L.Polygon(L.QuickHull.getConvexHull(points), {color: features[0].properties.color}).addTo(map);
    }
   	return new L.DivIcon({ html: '<div style="background:'+ features[0].properties.color +';"><span>' + childCount + '</span></div>',
       className: 'marker-cluster' + c,
       iconSize: new L.Point(40, 40) });	
  }

  private addClaster(features) {
     let markers = new L.MarkerClusterGroup({
        iconCreateFunction: (cluster) => this.createClasterIcon(cluster, features)
      });
      let customIcon = L.Icon.extend({
        options:{
          iconSize:     [42, 45], 
          iconAnchor:   [21, 45], 
          popupAnchor:  [0, -45]
        }
      });
      for (let feature of features) {
        let svgicon ="<svg xmlns='http://www.w3.org/2000/svg' width='50px' height='50px'><path d='M10.7,4.5C7.4999914,4.5 4.9000191,7.3000183 4.9000191,10.800018 4.9000191,14.300018 7.4999914,17.100006 10.7,17.100006 13.900009,17.100006 16.499982,14.300018 16.499982,10.800018 16.599957,7.3000183 13.999984,4.5 10.7,4.5z M10.7,0C16.599957,0 21.400001,5.2000122 21.400001,11.5 21.400001,17.899994 10.7,32 10.7,32 10.7,32 5.5881173E-08,17.899994 0,11.5 5.5881173E-08,5.2000122 4.7999825,0 10.7,0z' fill='" + feature.properties.color +"'/></svg>"
        let url = encodeURI("data:image/svg+xml," + svgicon).replace(/%20/g, ' ').replace(/%22/g, "'").replace(/%3C/g,'<').replace(/%3E/g,'>');
        let icon = new customIcon({iconUrl: url})
        let popupContent = "";
        if (feature.properties && feature.properties.popupContent) {
		      popupContent += feature.properties.popupContent;
		    }
        markers.addLayer(
          L.marker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {icon: icon})
            .bindPopup(popupContent));
      }
      map.addLayer(markers);
      map.setView([features[0].geometry.coordinates[1], features[0].geometry.coordinates[0]], defaultZoom);
      console.log(map);
  }
}